#include <TridentTD_LineNotify.h>
#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <DHT.h>
#define DHTPIN 5
#define DHTTYPE DHT22
#define LINE_TOKEN "yWMvVNGfiZA4RoTKDQ91p5qFa1QDNZsr9aokwxkcX4D"
DHT dht(DHTPIN, DHTTYPE);
char auth[] = "nN20qgOriYnAPWELbkilhcYt0ZpWerO_";
char ssid[] = "eieiza007";
char pass[] = "88888888";
BlynkTimer timer;
WidgetLCD lcd1(V3);
int delay1 = 4;
int delay2 = 14;
float w;
float h;
int count;
int countNubOne = 0;
int countNubTwo = 0;
int countNubThree = 0;
int countNubFour = 0;
int buttonState;
int btnMan ;
void sendSensor()
{
  Blynk.virtualWrite(V5, h);
}
void setup() {
  Serial.begin(9600);
  dht.begin();
  pinMode(delay1, OUTPUT);
  pinMode(delay2, OUTPUT);
  Blynk.begin(auth, ssid, pass);
  timer.setInterval(1000L, sendSensor);
  LINE.setToken(LINE_TOKEN);  // กำหนด Line Token
}

void loop() {
  Blynk.run();
  timer.run();
  float humidity = dht.readHumidity();
  int sensorValue;
  sensorValue = analogRead(A0);
  sensorValue = map(sensorValue, 0, 1023, 0, 100);
  w = sensorValue;
  h = humidity;
  lcd1.print(0, 0,  String("Water: ") + String(w));
  lcd1.print(0, 1,  String("Humidity: ") + String(h) + String(" RH") );
  if ( buttonState == 0) {
    count = 4;
    Serial.println(btnMan);
    Serial.println(digitalRead(delay2));
    if (btnMan == 1 ) {
      digitalWrite(delay1, HIGH);
      digitalWrite(delay2, LOW);
      delay(7000);
      digitalWrite(delay1, LOW);
      Serial.println("เก็บผ้า");

    } else if (btnMan == 0) {
      digitalWrite(delay2, HIGH);
      digitalWrite(delay1, LOW);
      Serial.println("ตากผ้า");
      delay(7000);
      digitalWrite(delay2, LOW);
    }
  } else {
    if (w >= 40) {
      digitalWrite(delay1, HIGH);
      digitalWrite(delay2, LOW);
      count = 1;
      delay(7000);
      digitalWrite(delay1, LOW);
    } else {
      if (h >= 70) {
        digitalWrite(delay2, HIGH);
        digitalWrite(delay1, LOW);
        count = 2;
        delay(7000);
        digitalWrite(delay2, LOW);
      } else {
        digitalWrite(delay1, HIGH);
        digitalWrite(delay2, LOW);
        count = 3;
        delay(7000);
        digitalWrite(delay1, LOW);
      }
    }
  }
  checkLine(count);
}
BLYNK_WRITE(V1) {
  buttonState = param.asInt();
  digitalWrite(delay2, LOW);
  digitalWrite(delay1, LOW);
}
BLYNK_WRITE(V2) {

  btnMan = param.asInt();

}
void checkLine(int c) {
  String txt = "";
  if (c == 1 && countNubOne == 0) {
    txt  = ("สถานะราวตากผ้า: ตรวจสอบพบมีน้ำมาสัมผัส กำลังเก็บผ้า");
    countNubOne = countNubOne + 1;
    countNubTwo = 0;
    countNubThree = 0;
    countNubFour = 0;
  } else if (c == 2 && countNubTwo == 0) {
    txt  = ("สถานะราวตากผ้า: ตรวจสอบพบผ้ายังไม่แห้ง กำลังตากผ้า");
    countNubTwo = countNubTwo + 1;
    countNubOne = 0;
    countNubThree = 0;
    countNubFour = 0;
  } else if (c == 3 && countNubThree == 0) {
    txt  = ("สถานะราวตากผ้า: ตรวจสอบพบผ้าแห้งเรียบร้อยแล้ว กำลังเก็บผ้า");
    countNubThree = countNubThree + 1;
    countNubOne = 0;
    countNubTwo = 0;
    countNubFour = 0;
  } else if (c == 4 && countNubFour == 0) {
    txt  = ("สถานะราวตากผ้า: สั่งงานผ่าน manual");
    countNubFour = countNubFour + 1;
    countNubOne = 0;
    countNubTwo = 0;
    countNubThree = 0;
  }
  LINE.notify(txt);
  count = 0;
}
